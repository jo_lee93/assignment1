import java.util.Scanner;

public class AssignmentOne {
	
	public static void main (String[] Args) {
		
		int n1, n2,count;	
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter your first integer: ");		
		n1 = keyboard.nextInt();
		
		System.out.println("Enter your second integer: ");		
		n2 = keyboard.nextInt();
		
		if (n1<n2) {
			
			count = n2-n1+1;			
			System.out.print("There are "+count+" integers between "+n1+ " and " +n2+ ": ");
			
			for (int i=n1;i<=n2;i++)				
				System.out.print(i+" ");
		} 
		
		else if(n2<n1) {
			
			count = n1-n2+1;			
			System.out.print("There are "+count+" integers between "+n2+ " and " +n1+ ": ");
			
			for (int i=n2;i<=n1;i++)				
				System.out.print(i+" ");
		} 
		
		else {
			
			System.out.print("You have entered the same integer. Please try again.");
			
		}	
		
	}

}
